#!/bin/bash

set -eu

export NODE_ENV=production

: ${XMPP_PORT:="0"}
if [[ "${XMPP_PORT}" == "0" ]]; then
    echo "=> Disabling XMPP support"
    XMPP_ENABLED=false
else
    echo "=> Enabling XMPP support on ${CLOUDRON_APP_DOMAIN}:${XMPP_PORT}"
    XMPP_ENABLED=true

    # generate cert for the domain
    if [[ ! -f "/app/data/cert/${CLOUDRON_APP_DOMAIN}.crt" ]]; then
        mkdir -p "/app/data/cert"
        echo "=> Generating self-signed cert for ${CLOUDRON_APP_DOMAIN}"
        openssl req -x509 -sha256 -newkey rsa:2048 -keyout /app/data/cert/${CLOUDRON_APP_DOMAIN}.key -out /app/data/cert/${CLOUDRON_APP_DOMAIN}.crt -days 5000 -nodes -subj "/CN=${CLOUDRON_APP_DOMAIN}"
    fi

    openssl x509 -noout -in /app/data/cert/${CLOUDRON_APP_DOMAIN}.crt -fingerprint
fi

echo "=> Configuring the app"
sed -e "s!##MONGODB_URL!${CLOUDRON_MONGODB_URL}!" \
    -e "s!##LDAP_URL!${CLOUDRON_LDAP_URL}!" \
    -e "s!##LDAP_USERS_BASE_DN!${CLOUDRON_LDAP_USERS_BASE_DN}!" \
    -e "s!##APP_DOMAIN!${CLOUDRON_APP_DOMAIN}!" \
    -e "s!##XMPP_PORT!${XMPP_PORT}!" \
    -e "s!##XMPP_ENABLED!${XMPP_ENABLED}!" \
    /app/code/settings.yml.template > /run/letschat/settings.yml

echo "=> Ensuring runtime directories"
mkdir -p /app/data/uploads /run/letschat/builtAssets
chown -R cloudron:cloudron /app/data /run

echo "=> Run lets chat"
# npm run-script migrate (this is done automatically as part of prestart script
exec /usr/local/bin/gosu cloudron:cloudron npm start
