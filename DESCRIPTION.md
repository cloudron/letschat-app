This app packages Lets Chat version 0.4.8

Lets Chat is a chat app for small teams.

### Features

* Persistent messages
* Multiple rooms
* Private and password-protected rooms
* New message alerts / notifications
* Mentions (hey @you/@all)
* Image embeds / Giphy search
* Code pasting
* File uploads
* Transcripts / chat history
* XMPP Multi-user chat (MUC)
* 1-to-1 chat between XMPP users
* REST-like API
* Basic i18n support
* MIT Licensed

