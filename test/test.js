#!/usr/bin/env node
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver'),
    url = require('url'),
    XmppClient = require('node-xmpp-client');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();
    var LOCATION = 'test';
    var app, pasteUrl;

    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function login(done) {
        browser.get('https://' + app.fqdn + '/login').then(function () {
            return browser.findElement(by.xpath('//form//input[@name="username"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//form//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[contains(text(), "All Rooms")]')), 4000);
        }).then(function () {
            done();
        });
    }

    function xmppLogin(done) {
        var client = new XmppClient({
            jid: username + '@' + app.fqdn,
            password: password,
            host: app.fqdn,
            port: 5222,
            disallowTLS: false
        });

        client.on('online', function () { console.log('xmpp online'); client.end(); done(); });
        client.on('error', function (error) { console.log('xmpp error', error); client.end(); done(error); });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can login', login);
    it('can login via XMPP', xmppLogin);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('can login via XMPP', xmppLogin);

    it('move to different location', function (done) {
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');

        done();
    });

    it('can login', login);
    it('can login via XMPP', xmppLogin);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install from appstore', function () {
        execSync('cloudron install --new --wait --appstore-id io.github.sdelements.lets_chat.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can login', login);
    it('can login via XMPP', xmppLogin);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can login', login);
    it('can login via XMPP', xmppLogin);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
