FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y libkrb5-dev && rm -rf /var/cache/apt /var/lib/apt/lists

RUN curl -L https://github.com/sdelements/lets-chat/archive/617207ff3c0c0bf8e3c7a915bd9ec03f1dd8390c.tar.gz | tar -xz --strip-components 1 -f -
RUN npm install --production
RUN npm install lets-chat-ldap

RUN mkdir -p /run/letschat/builtAssets
RUN ln -s /run/letschat/settings.yml /app/code/settings.yml && \
    ln -s /run/letschat/builtAssets /app/code/builtAssets

ADD settings.yml.template start.sh app.js /app/code/

CMD [ "/app/code/start.sh" ]
